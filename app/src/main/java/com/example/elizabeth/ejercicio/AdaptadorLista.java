package com.example.elizabeth.ejercicio;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.PipedOutputStream;
import java.util.List;

/**
 * Created by elizabeth on 02/04/2017.
 */

public class AdaptadorLista extends ArrayAdapter<String> {
    public AdaptadorLista(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = li.inflate(R.layout.item_lista,parent,false );
        TextView tvid = (TextView) row.findViewById(R.id.textView);
        TextView tv = (TextView) row.findViewById(R.id.textView2);
        tvid.setText(""+(int) getItemId(position));
        tv.setText(getItem(position));
        return row;
    }
}
