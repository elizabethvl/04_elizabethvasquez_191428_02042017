package com.example.elizabeth.ejercicio;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.DialogInterface;

import java.util.ArrayList;

public class MainActivity extends ListActivity {

    AdaptadorLista al;
    ArrayList<String> datos;
    ArrayList<Integer> id_list;
    Button btn;
    Button editar;
    Button eliminar;
    TextView tv;
    SQLiteDatabase db;
    int error = 0;
    int error2 = 0;

    public void abrirBaseDatos() {
        db = openOrCreateDatabase("db_lista", MODE_PRIVATE, null);
        db.execSQL("DROP TABLE tabla_lista");
        db.execSQL("CREATE TABLE IF NOT EXISTS tabla_lista(id int,nombre VARCHAR);");

    }

    public void insetarDatos(String nombre) {
        int i = 0;
        Cursor c = db.rawQuery("SELECT * FROM tabla_lista ORDER BY id", null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (i != c.getInt(c.getColumnIndex("id"))) {
                db.execSQL("INSERT INTO tabla_lista VALUES (" + i + ",'" + nombre + "');");
            }
            i++;
            c.moveToNext();
        }
        db.execSQL("INSERT INTO tabla_lista VALUES (" + i + ",'" + nombre + "');");
    }

    public void cargarDatos() {
        Cursor c = db.rawQuery("SELECT * FROM tabla_lista ORDER BY id", null);
        c.moveToFirst();
        id_list.clear();
        al.clear();
        while (!c.isAfterLast()) {
            id_list.add(c.getInt(c.getColumnIndex("id")));
            al.add(c.getString(c.getColumnIndex("nombre")));
            c.moveToNext();
        }
    }

    public void editarDatos(int x, String nuevonombre) {
        db.execSQL("UPDATE tabla_lista " + "SET nombre='" + nuevonombre + "' " + "WHERE id=" + x + ";");
    }

    public void borrarDatos(int x) {
        db.execSQL("DELETE FROM tabla_lista " + "WHERE id=" + x + ";");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datos = new ArrayList<String>();
        id_list = new ArrayList<Integer>();
        al = new AdaptadorLista(this, android.R.layout.simple_list_item_1, datos);
        setListAdapter(al);
        abrirBaseDatos();
        cargarDatos();

        tv = (TextView) findViewById(R.id.editText);
        btn = (Button) findViewById(R.id.button);
        editar = (Button) findViewById(R.id.edit);
        eliminar = (Button) findViewById(R.id.delete1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv.getText().length() > 0 && !tv.getText().toString().equals("")) {
                    insetarDatos(tv.getText().toString());
                    cargarDatos();
                    tv.setText("");

                }

            }
        });
        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setTitle("Editar");
                builder.setMessage("Ingrese el numero del elemento que desea editar");


                final EditText num = new EditText(MainActivity.this);
                builder.setView(num);

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       /* while(error==0){
                            try {
                                Integer val = Integer.valueOf(num.getText().toString());
                                if (val != null)
                                    error2=0;
                                else
                                    error2=1;
                            } catch (NumberFormatException e) {
                                error2=1;
                            }
                        }*/

                        AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);

                        builder2.setTitle("Editar");
                        builder2.setMessage("Ingrese el nuevo nombre");


                        final EditText nvonom = new EditText(MainActivity.this);
                        builder2.setView(nvonom);
                        builder2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (nvonom.getText().length() > 0 && !nvonom.getText().equals("")) {
                                    editarDatos(Integer.parseInt(num.getText().toString()), nvonom.getText().toString());
                                    cargarDatos();
                                } else {
                                    error = 1;
                                }
                            }

                        });

                        builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        });
                        builder2.show();
                    }

                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                builder.show();
            }
        });
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder3 = new AlertDialog.Builder(MainActivity.this);

                builder3.setTitle("Eliminar");
                builder3.setMessage("Ingrese el numero del elemento que desea eliminar");


                final EditText num = new EditText(MainActivity.this);
                builder3.setView(num);
                builder3.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        borrarDatos(Integer.parseInt(num.getText().toString()));
                        cargarDatos();
                    }

                });

                builder3.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                builder3.show();
            }
        });
    }

}


