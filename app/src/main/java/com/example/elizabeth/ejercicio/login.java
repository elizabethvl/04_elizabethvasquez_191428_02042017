package com.example.elizabeth.ejercicio;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Application;
import android.content.Context;

public class login extends AppCompatActivity {

    private SQLiteDatabase db;
    private Cursor c;
    private Button inicio;
    private String input_usuario;
    private String input_contraseña;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        db= openOrCreateDatabase("db_inicio",MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS tabla_login(id int,usuario VARCHAR , contraseña VARCHAR ,PRIMARY KEY (id));");
        db.execSQL("DELETE FROM tabla_login;");

        db.execSQL("INSERT INTO tabla_login VALUES(0,'eliza','vasq');");

        inicio= (Button)findViewById(R.id.iniciar);
        final TextView us= (TextView) findViewById(R.id.user);
        final TextView contra= (TextView) findViewById(R.id.password);

        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int error=0;
                input_usuario= us.getText().toString();
                input_contraseña = contra.getText().toString();
                c= db.rawQuery("SELECT * FROM tabla_login ORDER BY id",null);
                c.moveToFirst();
                while(!c.isAfterLast())
                {

                    if ( input_usuario.equals(c.getString(c.getColumnIndex("usuario"))))
                    {

                        if (input_contraseña.equals(c.getString(c.getColumnIndex("contraseña"))))
                        {
                            Intent intent= new Intent(login.this,MainActivity.class);
                            startActivity(intent);
                            error=1;
                        }
                    }
                    c.moveToNext();
                }
                if (error==0)
                {
                    Toast.makeText(getApplicationContext(),"Información incorrecta",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
